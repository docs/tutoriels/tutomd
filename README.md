# Tutoriel Markdown en français

## Origine

Ce tutoriel est une traduction en français du [tutoriel Markdown de CommonMark](https://commonmark.org/help/tutorial/), proposée par Arthur Perret (janvier 2020).

Source du code : <https://github.com/commonmark/commonmark-web/tree/gh-pages/help>

## Licence

- [MIT](https://fr.wikipedia.org/wiki/Licence_MIT)

## À propos d'Arthur Perret

[arthurperret.fr](https://www.arthurperret.fr/)

Arthur Perret est maître de conférences en sciences de l’information et de la communication à l’IUT de l’Université Jean Moulin Lyon 3, membre d’ELICO et chercheur associé au MICA.
